declare global {
  interface Window {
    doNotTrack: boolean | number | string | undefined;
  }
  interface Navigator {
    msDoNotTrack: boolean | number | string | undefined;
  }
  interface External {
    msTrackingProtectionEnabled: (() => boolean) | undefined;
  }
}

export {};
