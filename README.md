# GitLab Application SDK - Browser

This SDK is for usage of GitLab Application Services with vanilla Javascript in Browser

## How to use the Browser-SDK

Usage information for the browser sdk is documented in the [GitLab docs](https://docs.gitlab.com/ee/user/product_analytics/instrumentation/browser_sdk.html).

## Devkit

If you want to use the Browser SDK with [devkit](https://gitlab.com/gitlab-org/analytics-section/product-analytics/devkit), follow [devkit-developement guide](https://gitlab.com/gitlab-org/analytics-section/product-analytics/gl-application-sdk-js/-/blob/main/docs/devkit-development.md).

## Contribute

Want to contribute to Browser-SDK? follow [contributing guide](https://gitlab.com/gitlab-org/analytics-section/product-analytics/gl-application-sdk-js/-/blob/main/docs/Contributing.md).

## Run Vanilla JS Example

To run Vanilla JS example run below commands:

```bash
yarn examples:install
yarn examples:serve
```
