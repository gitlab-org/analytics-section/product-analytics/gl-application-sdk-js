import { glClientSDK } from '../../../dist/gl-sdk.min';
import { cookieBanner } from './cookieBanner';

const SNOWPLOW_COLLECTOR_URL = process.env.PA_COLLECTOR_URL;
const APPLICATION_ID = process.env.PA_APPLICATION_ID;

const gitlabSDK = glClientSDK({
  appId: APPLICATION_ID,
  host: SNOWPLOW_COLLECTOR_URL,
  plugins: {
    errorTracking: true,
  },
});

function addCookieConsent() {
  gitlabSDK.addCookieConsent();
}

cookieBanner.init(addCookieConsent);

gitlabSDK.page();

document.getElementById('testClickBtn').onclick = function () {
  gitlabSDK.track('custom_event', {
    key1: 'value1',
  });
};

document.getElementById('trackErrorBtn').onclick = function () {
  gitlabSDK.trackError({
    message: 'failed to load resource',
    filename: 'index.js',
    lineno: 30,
    error: new Error('failed to load resource'),
  });
};

document.getElementById('testIdBtn').onclick = function () {
  gitlabSDK.identify('123', {
    userRandomProps: 'Value',
  });
};
