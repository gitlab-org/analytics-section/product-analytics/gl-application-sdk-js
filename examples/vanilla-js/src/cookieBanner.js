export const cookieBanner = {
  init(addCookieConsent) {
    document.querySelector('.accept-btn').addEventListener('click', () => {
      addCookieConsent();
      this.hide();
    });

    document.querySelector('.decline-btn').addEventListener('click', this.hide);
  },

  hide() {
    document.querySelector('.cookie-consent-banner').style.display = 'none';
  },
};
