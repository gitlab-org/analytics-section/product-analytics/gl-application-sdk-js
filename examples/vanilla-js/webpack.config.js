const { EnvironmentPlugin } = require('webpack');

module.exports = {
  mode: 'development',
  plugins: [
    new EnvironmentPlugin({
      PA_COLLECTOR_URL: 'localhost:9091',
      PA_APPLICATION_ID: '123'
    })
  ]
};
