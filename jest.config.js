const config = {
  testEnvironment: 'jsdom',
  verbose: true,
  transform: {
    '^.+\\.(ts|js)$': 'ts-jest',
  },
};

module.exports = config;
