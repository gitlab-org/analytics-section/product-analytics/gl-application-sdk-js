import { ClientHintsPlugin } from '@snowplow/browser-plugin-client-hints';
import { LinkClickTrackingPlugin } from '@snowplow/browser-plugin-link-click-tracking';
import { PerformanceTimingPlugin } from '@snowplow/browser-plugin-performance-timing';
import { ErrorTrackingPlugin } from '@snowplow/browser-plugin-error-tracking';
import { TrackerConfiguration } from '@snowplow/browser-tracker-core';

import { AllowedPlugins } from '../types';
import { MAX_LOCAL_STORAGE_QUEUE_SIZE } from './constants';

export function getSnowplowOptions(
  appId: string,
  hasCookieConsent: boolean,
  allowedPlugins: AllowedPlugins
): TrackerConfiguration {
  const pluginFunctions: any = {
    clientHints: ClientHintsPlugin,
    linkTracking: LinkClickTrackingPlugin,
    performanceTiming: PerformanceTimingPlugin,
    errorTracking: ErrorTrackingPlugin,
  };

  const plugins = Object.entries(allowedPlugins)
    .filter(([, isEnabled]) => isEnabled)
    .map(([pluginName]) => pluginFunctions[pluginName]());

  const snowplowOptions = {
    appId,
    plugins,
    maxLocalStorageQueueSize: MAX_LOCAL_STORAGE_QUEUE_SIZE,
    customHeaders: {
      'X-GitLab-AppId': appId,
    },
    contexts: {
      webPage: true,
      browser: true,
    },
  };

  if (hasCookieConsent) {
    return snowplowOptions;
  }

  // If there is no cookie consent, enable anonymous tracking with cookieless experience
  // https://docs.snowplow.io/docs/collecting-data/collecting-from-own-applications/javascript-trackers/browser-tracker/browser-tracker-v3-reference/tracker-setup/initialization-options/#anonymous-tracking
  return {
    ...snowplowOptions,
    anonymousTracking: { withServerAnonymisation: true },
    stateStorageStrategy: 'none',
    eventMethod: 'post',
  };
}
