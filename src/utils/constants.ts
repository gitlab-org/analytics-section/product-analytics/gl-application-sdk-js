export const SCHEMAS = Object.freeze({
  CUSTOM_EVENT: 'iglu:com.gitlab/custom_event/jsonschema/1-0-0',
  USER_CONTEXT: 'iglu:com.gitlab/user_context/jsonschema/1-0-0',
});

export const DEFAULT_TRACKER_ID = 'gitlab';

export const DEFAULT_PAGEPING_OPTIONS = Object.freeze({
  minimumVisitLength: 30,
  heartbeatDelay: 30,
});

export const MAX_LOCAL_STORAGE_QUEUE_SIZE = 100;
