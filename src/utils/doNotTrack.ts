// Based on checks defined in https://github.com/DavidWells/analytics/blob/master/packages/analytics-plugin-do-not-track/src/index.js
export function doNotTrackEnabled(): boolean {
  // Not running a browser
  if (typeof window === 'undefined') return false;

  const { doNotTrack, navigator } = window;
  const dntSetting: boolean | number | string | undefined =
    doNotTrack ||
    navigator.doNotTrack ||
    navigator.msDoNotTrack ||
    msTrackingProtection();

  if (!dntSetting) {
    return false;
  }

  return (
    dntSetting === true ||
    dntSetting === 1 ||
    dntSetting === 'yes' ||
    (typeof dntSetting === 'string' && dntSetting.charAt(0) === '1')
  );
}

function msTrackingProtection(): boolean | undefined {
  const { external } = window;
  return (
    typeof external !== 'undefined' &&
    'msTrackingProtectionEnabled' in external &&
    typeof external.msTrackingProtectionEnabled === 'function' &&
    external.msTrackingProtectionEnabled?.()
  );
}
