import {
  ActivityTrackingConfiguration,
  BrowserTracker,
  PageViewEvent,
  CommonEventProperties,
} from '@snowplow/browser-tracker';

/**
 *  Boolean for allowed plugins, by default all plugins are enabled
 */
export interface AllowedPlugins {
  clientHints?: boolean;
  linkTracking?: boolean;
  performanceTiming?: boolean;
  errorTracking?: boolean;
}

/**
 * Gitlab Client SDK Options
 */
export interface GitLabClientSDKOptions {
  /** The app id to send with each event */
  appId: string;
  /** Collector endpoint in the form collector.mysite.com */
  host: string;
  /** Consent from the user on read/write cookies */
  hasCookieConsent?: boolean;
  /** The tracker id - also known as tracker namespace */
  trackerId?: string;
  /** If page ping should be enabled or The base configuration for page ping */
  pagePingTracking?: boolean | ActivityTrackingConfiguration;
  /** config to enable disable plugins */
  plugins?: AllowedPlugins;
}

/**
 * Event for tracking an error
 */
export interface ErrorEventProperties {
  /** The error message */
  message: string;
  /** The filename where the error occurred */
  filename?: string;
  /** The line number which the error occurred on */
  lineno?: number;
  /** The column number which the error occurred on */
  colno?: number;
  /** The error object */
  error?: Error;
}

/**
 * Gitlab SDK instance
 */
export type GitLabClientSDKType = {
  /** Track the user defined custom event */
  track: (eventName: string, eventAttributes?: Record<string, any>) => void;
  /** Send error as self-describing event */
  trackError: (
    errorAttributes: ErrorEventProperties & CommonEventProperties
  ) => void;
  /** Set cookie consent */
  addCookieConsent: () => void;
  /**  Tracks the page views */
  page: (event?: PageViewEvent & CommonEventProperties) => void;
  /** Set the business defined userId and other user attributes */
  identify: (userId: string, userAttributes?: Record<string, any>) => void;
  /** Set a custom URL for tracking */
  setCustomUrl: (url: string) => void;
  /** Set a referrer URL for tracking */
  setReferrerUrl: (url: string) => void;
  /** Override a document title */
  setDocumentTitle: (title: string) => void;
};
