import { nodeResolve } from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import terser from '@rollup/plugin-terser';
import pkg from './package.json' assert { type: 'json' };
import dts from 'rollup-plugin-dts';
import esbuild from 'rollup-plugin-esbuild';

const name = pkg.main.replace(/\.js$/, '');
const bundle = (config) => ({
  ...config,
  input: 'src/main.ts',
});

export default [
  bundle({
    plugins: [
      nodeResolve({
        browser: true,
        extensions: ['.ts', '.js'],
      }),
      commonjs(),
      esbuild(),
      terser(),
    ],
    output: [
      {
        file: pkg.main,
        format: 'cjs',
        sourcemap: false,
      },
      {
        file: pkg.module,
        format: 'es',
        sourcemap: false,
      },
    ],
  }),
  bundle({
    output: [
      {
        file: pkg.browser,
        format: 'umd',
        name: 'glSDK',
        sourcemap: false,
      },
    ],
    plugins: [
      nodeResolve({
        browser: true,
      }),
      commonjs(),
      esbuild(),
      terser(),
    ],
  }),
  bundle({
    plugins: [dts()],
    output: {
      file: `${name}.d.ts`,
      format: 'es',
    },
  }),
];
