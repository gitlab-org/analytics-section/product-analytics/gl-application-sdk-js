import { doNotTrackEnabled } from '../../src/utils/doNotTrack';

declare global {
  interface Window {
    doNotTrack: boolean | number | string | undefined;
  }
  interface Navigator {
    msDoNotTrack: boolean | number | string | undefined;
  }
}

describe('running in browser', () => {
  it('returns false by default', () => {
    expect(doNotTrackEnabled()).toBe(false);
  });

  it('returns true if window.doNotTrack is set', () => {
    Object.defineProperty(window, 'doNotTrack', {
      writable: true,
      value: true,
    });
    expect(doNotTrackEnabled()).toBe(true);
    window.doNotTrack = undefined;
  });

  it('returns true if navigator.doNotTrack is set', () => {
    Object.defineProperty(navigator, 'doNotTrack', {
      writable: true,
      value: true,
    });
    expect(doNotTrackEnabled()).toBe(true);
    Object.defineProperty(navigator, 'doNotTrack', {
      writable: true,
      value: undefined,
    });
  });

  it('returns true if navigator.msDoNotTrack is set', () => {
    Object.defineProperty(navigator, 'msDoNotTrack', {
      writable: true,
      value: true,
    });
    expect(doNotTrackEnabled()).toBe(true);
    navigator.msDoNotTrack = undefined;
  });
});
