import { isEmpty } from '../../src/utils/isEmpty';

const testData = [
  { description: 'empty objects', value: {}, expected: true },
  {
    description: 'non-empty objects',
    value: { key: 'value' },
    expected: false,
  },
  { description: 'empty strings', value: '', expected: true },
  { description: 'non-empty strings', value: 'Hello', expected: false },
  { description: 'empty arrays', value: [], expected: true },
  { description: 'non-empty arrays', value: [1, 2, 3], expected: false },
  { description: 'null values', value: null, expected: true },
  { description: 'undefined values', value: undefined, expected: true },
];

describe('isEmpty', () => {
  it.each(testData)(
    'should return $expected for $description',
    ({ value, expected }) => {
      expect(isEmpty(value)).toBe(expected);
    }
  );
});
