/**
 * @jest-environment node
 */
import { doNotTrackEnabled } from '../../src/utils/doNotTrack';
describe('running in node', () => {
  test('returns automatically false', () => {
    expect(doNotTrackEnabled()).toBe(false);
  });
});
