import {
  TrackerConfiguration,
  BuiltInContexts,
} from '@snowplow/browser-tracker-core';
import { ClientHintsPlugin } from '@snowplow/browser-plugin-client-hints';
import { LinkClickTrackingPlugin } from '@snowplow/browser-plugin-link-click-tracking';
import { PerformanceTimingPlugin } from '@snowplow/browser-plugin-performance-timing';
import { ErrorTrackingPlugin } from '@snowplow/browser-plugin-error-tracking';
import { getSnowplowOptions } from '../../src/utils/getSnowplowOptions';
import { AllowedPlugins } from '../../src/types';
import { MAX_LOCAL_STORAGE_QUEUE_SIZE } from '../../src/utils/constants';

jest.mock('@snowplow/browser-plugin-client-hints');
jest.mock('@snowplow/browser-plugin-link-click-tracking');
jest.mock('@snowplow/browser-plugin-performance-timing');
jest.mock('@snowplow/browser-plugin-error-tracking');

describe('getSnowplowOptions', () => {
  const appId = 'test-app';
  const allowedPlugins: AllowedPlugins = {
    clientHints: true,
    linkTracking: true,
    performanceTiming: true,
    errorTracking: true,
  };
  const contexts: BuiltInContexts = {
    webPage: true,
    browser: true,
  };

  it('returns options with all plugins enabled and hasCookieConsent is true', () => {
    const result = getSnowplowOptions(appId, true, allowedPlugins);

    const expectedResult: TrackerConfiguration = {
      appId,
      plugins: [
        ClientHintsPlugin(),
        LinkClickTrackingPlugin(),
        PerformanceTimingPlugin(),
        ErrorTrackingPlugin(),
      ],
      maxLocalStorageQueueSize: MAX_LOCAL_STORAGE_QUEUE_SIZE,
      customHeaders: {
        'X-GitLab-AppId': appId,
      },
      contexts,
    };

    expect(result).toEqual(expectedResult);
  });

  it('returns options with all plugins disabled and hasCookieConsent is true', () => {
    const result = getSnowplowOptions(appId, true, {
      clientHints: false,
      linkTracking: false,
      performanceTiming: false,
      errorTracking: false,
    });

    const expectedResult: TrackerConfiguration = {
      appId,
      plugins: [],
      contexts,
      maxLocalStorageQueueSize: MAX_LOCAL_STORAGE_QUEUE_SIZE,
      customHeaders: {
        'X-GitLab-AppId': appId,
      },
    };

    expect(result).toEqual(expectedResult);
  });

  it('returns options with some plugins enabled and hasCookieConsent is true', () => {
    const result = getSnowplowOptions(appId, true, {
      clientHints: false,
      linkTracking: false,
      performanceTiming: true,
      errorTracking: false,
    });

    const expectedResult: TrackerConfiguration = {
      appId,
      plugins: [PerformanceTimingPlugin()],
      contexts,
      maxLocalStorageQueueSize: MAX_LOCAL_STORAGE_QUEUE_SIZE,
      customHeaders: {
        'X-GitLab-AppId': appId,
      },
    };

    expect(result).toEqual(expectedResult);
  });

  it('returns options with anonymous tracking when hasCookieConsent is false', () => {
    const result = getSnowplowOptions(appId, false, allowedPlugins);

    const expectedResult: TrackerConfiguration = {
      appId,
      plugins: [
        ClientHintsPlugin(),
        LinkClickTrackingPlugin(),
        PerformanceTimingPlugin(),
        ErrorTrackingPlugin(),
      ],
      anonymousTracking: { withServerAnonymisation: true },
      stateStorageStrategy: 'none',
      eventMethod: 'post',
      contexts,
      maxLocalStorageQueueSize: MAX_LOCAL_STORAGE_QUEUE_SIZE,
      customHeaders: {
        'X-GitLab-AppId': appId,
      },
    };
    expect(result).toEqual(expectedResult);
  });
});
