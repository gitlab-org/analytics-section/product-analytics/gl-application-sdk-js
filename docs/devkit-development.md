
## Developing with the devkit

To develop with a local Snowplow pipeline you can use the Analytics devkit's [snowplow setup](https://gitlab.com/gitlab-org/analytics-section/product-analytics/devkit/-/tree/main#setup).

### Setup the devkit

1. Run `curl -X POST http://localhost:4567/setup-project/example -u [username]:[password]` to set up Clickhouse for receiving events from the GDK. Use the [credentials for the analytics-configurator](https://gitlab.com/gitlab-org/analytics-section/product-analytics/devkit/-/tree/main#test-credentials).
1. Note down the `app_id` that is returned from the configurator.
1. Modify environment variables default values in `examples/vanilla-js/webpack.config.js` to point at your local devkit and project 
    ```javascript
    PA_COLLECTOR_URL: 'localhost:9091',
    PA_APPLICATION_ID: '[app_id  from configurator]'
    ```
    or set environment variables `PA_COLLECTOR_URL` & `PA_APPLICATION_ID` in your shell.
    ```bash
    export PA_COLLECTOR_URL='localhost:9091'
    export PA_APPLICATION_ID='<app_id from configurator>'
    ```
4. If you haven't yet, build the Browser SDK with `yarn build` at the root of `gl-application-sdk-browser`.
1. Run the example by running `yarn serve` within `examples/vanilla-js`
1. Go to http://localhost:8080 to press the buttons on the example page

### Seeing the events in Clickhouse

1. Go to http://localhost:8123/play and use the [credentials for Clickhouse](https://gitlab.com/gitlab-org/analytics-section/product-analytics/devkit/-/tree/main#test-credentials).
2. Then run your GDK e.g. with `gdk start`
3. Click around in your local GitLab version
4. Run `SELECT * from example.snowplow_events Order by collector_tstamp desc` to see the incoming events