# Developer guide

This guide helps you get started developing GitLab Application SDK - Browser.

## Semantic Versioning

The Browser-SDK follows [semantic versioning](https://semver.org/). We release patch versions for critical bugfixes, minor versions for new features or non-essential changes, and major versions for any breaking changes.

## Branch Organization

Submit all changes directly to the [main branch](https://gitlab.com/gitlab-org/analytics-section/product-analytics/gl-application-sdk-js/-/tree/main?ref_type=heads). We don’t use separate branches for development or for upcoming releases. We do our best to keep main in good shape, with all tests passing.

## Bugs

We are using [GitLab issues](https://gitlab.com/gitlab-org/analytics-section/product-analytics/gl-application-sdk-js/-/issues) for bugs and features. If something is not working properly, feel free to raise a bug there. Before filing a new bug, try to make sure your problem doesn’t already exist.

## Your First Merge Request

If you decide to fix an issue, please be sure to check the comment thread in case somebody is already working on a fix. If nobody is working on it at the moment, please leave a comment stating that you intend to work on it so other people don’t accidentally duplicate your effort.

Before submitting a merge request, please make sure the following is done:

- Clone the repository and create your branch from main.
- Run yarn in the repository root.
- If you’ve fixed a bug or added code that should be tested, add tests!
- Ensure the test suite passes (`yarn test`).

## Contribution Prerequisites

- You have [Node](https://nodejs.org/en) installed at LTS.
- You are familiar with Git.

## Development guidelines

- `yarn build` builds the npm packages and the classic browser library
- `yarn test` builds the packages and runs jest tests
- `yarn clean` cleans the dist folder
- `yarn lerna:publish` to publish a newly built package. You need to run `npm login` with your personal npm login before.
- If you want to use the Browser SDK with [devkit](https://gitlab.com/gitlab-org/analytics-section/product-analytics/devkit), follow [devkit-developement guide](https://gitlab.com/gitlab-org/analytics-section/product-analytics/gl-application-sdk-js/-/blob/main/docs/devkit-development.md).

## Use GitLab Application SDK in your web application

If you want to use the Browser SDK in your webapp follow [this guide](https://gitlab.com/gitlab-org/analytics-section/product-analytics/gl-application-sdk-js/-/blob/main/README.md)

## Publishing to NPM

We use [Semantic Release](https://github.com/semantic-release/semantic-release) to publish new version. When any changes are merged to main it triggers a release and based on commit message it updates version to Patch, Minor or Major.

## How does it work?

### Commit message format

**semantic-release** uses the commit messages to determine the consumer impact of changes in the codebase.
Following formalized conventions for commit messages, **semantic-release** automatically determines the next [semantic version](https://semver.org) number, generates a changelog and publishes the release.

By default, **semantic-release** uses [Angular Commit Message Conventions](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#-commit-message-format).
The commit message format can be changed with the [`preset` or `config` options](docs/usage/configuration.md#options) of the [@semantic-release/commit-analyzer](https://github.com/semantic-release/commit-analyzer#options) and [@semantic-release/release-notes-generator](https://github.com/semantic-release/release-notes-generator#options) plugins.

Tools such as [commitizen](https://github.com/commitizen/cz-cli) or [commitlint](https://github.com/conventional-changelog/commitlint) can be used to help contributors and enforce valid commit messages.

The table below shows which commit message gets you which release type when `semantic-release` runs (using the default configuration):

| Commit message                                                                                                                                                                                   | Release type                                                                                                    |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------------------- |
| `fix(pencil): stop graphite breaking when too much pressure applied`                                                                                                                             | ~~Patch~~ Fix Release                                                                                           |
| `feat(pencil): add 'graphiteWidth' option`                                                                                                                                                       | ~~Minor~~ Feature Release                                                                                       |
| `perf(pencil): remove graphiteWidth option`<br><br>`BREAKING CHANGE: The graphiteWidth option has been removed.`<br>`The default graphite width of 10mm is always used for performance reasons.` | ~~Major~~ Breaking Release <br /> (Note that the `BREAKING CHANGE: ` token must be in the footer of the commit) |
